<?php
//total page count = 27000
function loadPages(&$pages, &$page_count, &$corrupted){
  for($i = 0; $i < $page_count; $i++){
    exec('phantomjs page.js http://www.sportsporudy.gov.ua/catalog/view/id/'.($i + 1), $out, $in);
    if(strlen($out[$i]) > 36){
      array_push($pages, $out[$i]);
      printf("%d is parsed...\n", $i);
    }
    else{
      $corrupted++;
      printf("%d is corrupted...\n", $i);
    }
  }
}

function DomNodeListToArray($nodeList){
  $arr = array();
  foreach($nodeList as $elem){
    array_push($arr, str_replace(array("\\t", "\\n", "\\", "\""), '', $elem->nodeValue));
  }
  return $arr;
}

function save($data){
  $fp = fopen("data.json", "a");
  fwrite($fp, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
  fclose($fp);
}

function queryData($htmlContent, $xpathQuery){
  $doc = new DOMDocument();
  libxml_use_internal_errors(true);
  $doc->loadHTML($htmlContent);
  libxml_clear_errors();
  return (new DOMXPath($doc))->query($xpathQuery);
}

$time_pre = microtime(true);
$corrupted = 0;
$page_count = 10;
$htmlPages = array();
loadPages($htmlPages, $page_count, $corrupted);
$keys = DomNodeListToArray(queryData($htmlPages[0],'//tbody//td[strong]'));

for($i = 0; $i < count($htmlPages); $i++){
  $values = DomNodeListToArray(queryData($htmlPages[$i],'//tbody//td[not(img) and not(strong)]'));
  if(count($keys) == count($values)){
    save(array_combine($keys, $values));
  }
  else{
    printf("%d page could not be parsed properly\n", $i);
    printf("keys %d != values %d\n", count($keys), count($values));
  }
}

$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
printf("%d entries have been parsed.\n%d entries are corrupted.\n", $page_count, $corrupted);
print_r($exec_time);
?>
