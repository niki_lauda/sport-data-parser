var page = require("webpage").create();
var url = require("system").args[1];

var system = require('system');

page.onConsoleMessage = function(msg) {
    system.stderr.writeLine('console: ' + msg);
};


function onPageReady() {
  var content = page.evaluate(function() {
    return document.documentElement.innerHTML;
  });
  console.log(JSON.stringify(content));
  phantom.exit();
}

page.open(url, function(status) {
  function checkReadyState() {
    setTimeout(function() {
      var readyState = page.evaluate(function() {
        return document.readyState;
      });
      if ('complete' === readyState) {
        onPageReady();
      } else {
        checkReadyState();
      }
    })
  }
  checkReadyState();
});
